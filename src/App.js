import React, { Component } from 'react';
import { Form, Text } from 'react-form'

import { Auth } from './api'
import Candle from './Candle'

class App extends Component {
  state = {
    isAuthenticated: Auth.getToken() ? true : false
  }

  render() {
    let { isAuthenticated } = this.state
    return (
      <div>
        {!isAuthenticated ?
          <Form
            onSubmit={values => {
              if (Auth.login(values)) {
                this.setState({
                  isAuthenticated: true
                })
              }
            }}
            defaultValues={{
              email: 'test@testing.com',
              password: 'testingpassword123!'
            }}
          >
            {formApi => (
              <form
                onSubmit={formApi.submitForm}
                id="login-form"
              >
                <label htmlFor="email">email</label>
                <Text field="email" id="email" />
                <br />
                <label htmlFor="password">password</label>
                <Text field="password" id="password" type="password" />
                <br />
                <button type="submit" className="btn btn-primary">
                  Login
                </button>
              </form>
            )}
          </Form>
          :
          <div
          >
            <button
              onClick={() => {
                Auth.signout()
                this.setState({
                  isAuthenticated: false
                })
              }}
            >sign out</button>
            <Candle />
          </div>
        }
      </div>
    );
  }

}

export default App;
