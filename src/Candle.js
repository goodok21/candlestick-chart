import React, { Component } from 'react';
import { Data } from './api'

class Candle extends Component {
  state = {
    candlestickData: {}
  }

  handleData = () => {
    return Data.getData().then(({ data }) => {
      this.setState({
        candlestickData: data.candlestick_data
      })
    })
  }

  render() {
    console.log(this.state);
    return (
      <div>
        <button onClick={this.handleData}>fetch</button>
        {this.state.candlestickData &&
          JSON.stringify(this.state.candlestickData)
        }
      </div>
    );
  }

}

export default Candle;
