import axios from 'axios'
const TOKEN = process.env.REACT_APP_TOKEN

const Auth = {
  getToken() {
    return localStorage.getItem(TOKEN)
  },
  login(values) {
    let token = this.getToken()
    if (token) {
      return true;
    } else {
      return axios.post(process.env.REACT_APP_AUTH_URL, values)
        .then(values => {
          console.log(values);
          if (values.data) {
            let token = values.data.access
            localStorage.setItem(TOKEN, token)
            return true;
          } else {
            return false;
          }
        })
    }
  },
  signout() {
    localStorage.removeItem(TOKEN)
  },
}

const Data = {
  getData() {
    return axios.get(process.env.REACT_APP_CANDLE_URL, {
      headers: {
        Authorization: `Bearer ${Auth.getToken()}`
      }
    })
  }
}

export {
  Auth,
  Data
}
